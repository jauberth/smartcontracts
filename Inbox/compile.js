const path = require('path'); // linhas para indicar o caminho onde o arquivo será lido
const fs = require('fs');    // e garantir a compatibilidade de sistemas operacionais
const solc = require('solc');

const inboxPath = path.resolve(__dirname, 'contracts', 'Inbox.sol');
const source = fs.readFileSync(inboxPath, 'utf8');

solc.compile(source,1); // numéro de contratos a serem complilados.
// comente este para desativar o log

//console.log(solc.compile(source,1)); // gera o log para investigação
// descomente este para ver o log

module.exports=solc.compile(source,1).contracts[':Inbox']; 
// pedimos apenas o nosso contrato para exportação
